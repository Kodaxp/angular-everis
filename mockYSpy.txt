Pruebas de objetos simulados (stub)
    --> reemplaza un componente y retorna lo que ese componente retornaria
    
pruebas de objeto simulado (mock)
    --> reemplaza un componente y retorna lo que ese componente retornaria, pero ademas modifica retorno de componentes
        internos del mock

SPY EN JASMINE

    espiar y reemplazar el resultado de un llamado      --> spyOn(someObj,'func').and.returnValue(42);
                                                        --> someObjt.func(1,2,3); //return 42
    
    espiar con argumentos                               --> spyOn(someObj,'func').withArgs(1,2,3).and.returnValue(42);
                                                        --> someObj.func(1,2,3); //return 42

ANGULAR TEST BED (ATB)

¿Que es ATB?
    --> es una virtualizacion de un modulo para pruebas

¿Que permite el ATB?
    --> la interaccion de directivas o componentes con su template
    --> pruebas de deteccion de cambio
    --> pruebas y usuos de inyeccion de dependencia de angular
    --> pruebas de las configuraciones que se hacen en un ngModule
    --> permite probar interacciones de usuarios via clic y campos de entrada

¿Como se usa un ATB?
    --> primero se declara un objeto                                                           --> ComponentFixure
    --> este es un componente virtual que se genera de una clase generica                      --> ComponentFixure<T>
    --> creamos un beforeEach con un metodo anonimo asincrono, dentro configurtamos el ATB     --> TestBed.configureTestingModule({
                                                                                                        declarations:[components],
                                                                                                        providers:[service]
                                                                                                    }).compliceComponents
    --> luego, dentro de otro beforeEach agregamos la instancia                                --> fixure = TestBed.createComponent(Component);
                                                                                                   component = fixure.componentInstance
                                                                                            
