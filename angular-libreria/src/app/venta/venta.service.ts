import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Libros } from '../libro/libros';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class VentaService {

  public URL_API = "http://localhost:3004/libros";

  constructor(private httpClient :HttpClient) { }
    
  listadoDeLibrosPorID(id):Observable<Libros[]>{
    return this.httpClient.get<Libros[]>(this.URL_API + '/' + id);
  }

}
