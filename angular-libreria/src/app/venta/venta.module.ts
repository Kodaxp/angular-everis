import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentaRoutingModule } from './venta-routing.module';
import { VentaComponent } from './venta.component';
import { ListadoVentaComponent } from './listado-venta/listado-venta.component';
import { CarritoVentaComponent } from './carrito-venta/carrito-venta.component';
import { VentaService } from './venta.service';
import { ComunService } from './comun.service';

@NgModule({
  imports: [
    CommonModule,
    VentaRoutingModule
  ],
  declarations: [VentaComponent, ListadoVentaComponent, CarritoVentaComponent],
  providers: [VentaService, ComunService]
})
export class VentaModule { }
