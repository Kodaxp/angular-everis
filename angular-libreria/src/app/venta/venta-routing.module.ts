import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoVentaComponent } from './listado-venta/listado-venta.component';
import { CarritoVentaComponent } from './carrito-venta/carrito-venta.component';

const routes: Routes = [
  {path:'venta', children:[
    {path:'listar', component:ListadoVentaComponent},
    {path:'carrito', component: CarritoVentaComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentaRoutingModule { }
