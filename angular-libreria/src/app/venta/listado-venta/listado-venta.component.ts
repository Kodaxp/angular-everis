import { Component, OnInit } from '@angular/core';
import { Libros } from '../../libro/libros';
import { LibrosService } from '../../libro/libros.service';
import { Router } from '@angular/router';
import { ComunService } from '../comun.service';
import { VentaService } from '../venta.service';

@Component({
  selector: 'app-listado-venta',
  templateUrl: './listado-venta.component.html',
  styleUrls: ['./listado-venta.component.css']
})
export class ListadoVentaComponent implements OnInit {

  public listadoLibros:Libros[];
  public libro:any[];


  constructor(private libroService: LibrosService, private router:Router, private ventaService: VentaService) { }

  ngOnInit() {
    this.libroService.listadoDeLibros().subscribe(
      (respuesta) => {
        this.listadoLibros = respuesta;
      }
    );
    this.listadoLibros = [];
    this.libro = [];
  }

  guardarLibro(id){
    let producto = new Libros();
    this.libroService.listadoDeLibrosPorID(id).subscribe(
      (respuesta) => {
        producto = respuesta;
        producto.cantidad = 1;
        console.log(producto);
        if (this.libro.length == 0) {
          this.libro.push(producto)
        }else{
          for (let index = 0; index < this.libro.length; index++) {
            if (this.libro[index].id == producto.id) {
              this.libro[index].cantidad++;
              break;
            }else if (index == this.libro.length - 1) {
              this.libro.push(producto);
              break;
            }
          }
        }
        console.log(this.libro);
        
        ComunService.guardarLibroEnSesion(this.libro);
      }
    );
    
  }

  
}

// this.ventaService.listadoDeLibrosPorID(id).subscribe(
//   (respuesta) => {
//     this.libro = respuesta;

//     for (let index = 0; index < this.libro.length; index++) {

//     }
//   }
// );