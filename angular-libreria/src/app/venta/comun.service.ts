import { Injectable } from '@angular/core';
import { Libros } from '../libro/libros';

  const NOMBRE_CARRITO = "carrito";

@Injectable()
export class ComunService {

  constructor() { }

  static guardar(nombre:string, usuario:string){
    localStorage.setItem(nombre, usuario);
  }

  static guardarLibroEnSesion(libro:Libros[]){
    return this.guardar(NOMBRE_CARRITO, JSON.stringify(libro));
  }

  static buscarProductoEnSesion():Libros[]{
    if(null == this.buscarProducto(NOMBRE_CARRITO)){
      return null;
    }
    let producto = JSON.parse(this.buscarProducto(NOMBRE_CARRITO));
    return producto;
  }

  static buscarProducto(nombre:string):string{
    return localStorage.getItem(nombre);
}

}
