import { Component, OnInit } from '@angular/core';
import { VentaService } from '../venta.service';
import { Libros } from '../../libro/libros';
import { ComunService } from '../comun.service';

@Component({
  selector: 'app-carrito-venta',
  templateUrl: './carrito-venta.component.html',
  styleUrls: ['./carrito-venta.component.css']
})
export class CarritoVentaComponent implements OnInit {
  
  public listaLibros:any[];
  public libros:Libros[];


  constructor(private ventaService: VentaService) { 
    this.libros = [];
    this.listaLibros = ComunService.buscarProductoEnSesion();
    for (let index = 0; index < this.listaLibros.length; index++) {
      console.log(this.listaLibros[index].titulo);
    }
  }

  ngOnInit() {
  }

}
