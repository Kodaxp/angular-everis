export class Libros {
    public id:Number;
    public titulo:String;
    public precio:Number;
    public idioma:String;
    public ISBN:Number;
    public descripcion:String;
    public imagen:Number;
    public cantidad:Number;
}
