import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { LibroRoutingModule } from './libro-routing.module';
import { LibroComponent } from './libro.component';
import { CrearLibroComponent } from './crear-libro/crear-libro.component';
import { ListarLibroComponent } from './listar-libro/listar-libro.component';
import { DetalleLibroComponent } from './detalle-libro/detalle-libro.component';
import { EditarLibroComponent } from './editar-libro/editar-libro.component';
import { LibrosService } from './libros.service';

@NgModule({
  imports: [
    CommonModule,
    LibroRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [LibroComponent, CrearLibroComponent, ListarLibroComponent, DetalleLibroComponent, EditarLibroComponent],
  providers: [LibrosService]
})
export class LibroModule { }
