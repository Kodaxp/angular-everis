import { Component, OnInit } from '@angular/core';
import { Libros } from '../libros';
import { LibrosService } from '../libros.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-libro',
  templateUrl: './listar-libro.component.html',
  styleUrls: ['./listar-libro.component.css']
})
export class ListarLibroComponent implements OnInit {

  public listadoLibros:Libros[];

  constructor(private libroService: LibrosService, private router:Router) { }

  ngOnInit() {
    this.libroService.listadoDeLibros().subscribe(
      (respuesta) => {
        this.listadoLibros = respuesta;
      }
    );
  }

  eliminarLibro(id){
    if (id > 0) {
      this.libroService.eliminarLibro(id).subscribe(
        (respuesta) => {
          console.log(respuesta); 
          this.libroService.listadoDeLibros().subscribe(
            (respuesta) => {
              this.listadoLibros = respuesta;
            }
          );
        }
      );
    }
  }

  editarLibro(id){
    this.router.navigate(['libro/editar', id]);
    console.log(id);
  }

}
