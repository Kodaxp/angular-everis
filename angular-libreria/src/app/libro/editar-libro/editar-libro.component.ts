import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Libros } from '../libros';
import { LibrosService } from '../libros.service';

@Component({
  selector: 'app-editar-libro',
  templateUrl: './editar-libro.component.html',
  styleUrls: ['./editar-libro.component.css']
})
export class EditarLibroComponent implements OnInit {

  public formularioEdicion:FormGroup;

  public id:Number;

  public libro:Libros;

  public titulo:FormControl;
  public precio:FormControl;
  public idioma:FormControl;
  public isbn:FormControl;
  public descripcion:FormControl;
  public idImagen:FormControl;
  public cantidad:FormControl;

  constructor(private route:ActivatedRoute, private libroService: LibrosService, private router:Router) { 
    this.titulo = new FormControl('', Validators.required);
    this.precio = new FormControl('', Validators.required);
    this.idioma = new FormControl('', Validators.required);
    this.isbn = new FormControl('', Validators.required);
    this.descripcion = new FormControl('', Validators.required);
    this.idImagen = new FormControl('', Validators.required);
    this.cantidad = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.route.params.forEach(
      (params:Params) => {
        console.log(params['id']);
        this.id = params['id'];
      }
    )

    this.formularioEdicion = new FormGroup({
      titulo: this.titulo,
      precio: this.precio,
      idioma: this.idioma,
      isbn: this.isbn,
      descripcion: this.descripcion,
      idImagen: this.idImagen,
      cantidad: this.cantidad
    });
    
    this.libroService.listadoDeLibrosPorID(this.id).subscribe(
      (respuesta) => {
        this.libro = respuesta;

        this.titulo.setValue(this.libro.titulo);
        this.precio.setValue(this.libro.precio);
        this.idioma.setValue(this.libro.idioma);
        this.isbn.setValue(this.libro.ISBN);
        this.descripcion.setValue(this.libro.descripcion);
        this.idImagen.setValue(this.libro.imagen);
        this.cantidad.setValue(this.libro.cantidad);
      }
    );
  }

  editarLibro(){
    if (this.formularioEdicion.valid) {
      this.libro.titulo = this.titulo.value;
      this.libro.precio = this.precio.value;
      this.libro.idioma = this.idioma.value;
      this.libro.ISBN = this.isbn.value;
      this.libro.descripcion = this.descripcion.value;
      this.libro.cantidad = this.cantidad.value;
      this.libro.imagen = this.idImagen.value;
      this.libroService.editarLibros(this.libro).subscribe(
        (respuesta) => {
          console.log(respuesta);
          this.router.navigate(['libro/listar']);
        }
      );
    }
  }

}
