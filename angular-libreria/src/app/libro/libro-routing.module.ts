import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearLibroComponent } from './crear-libro/crear-libro.component';
import { ListarLibroComponent } from './listar-libro/listar-libro.component';
import { AutentificacionGuard } from '../autentificacion/autentificacion.guard';
import { EditarLibroComponent } from './editar-libro/editar-libro.component';

const routes: Routes = [
  {path:'libro', children:[
    {path:'crear', canActivate:[AutentificacionGuard], component: CrearLibroComponent},
    {path:'listar', canActivate:[AutentificacionGuard], component: ListarLibroComponent},
    {path:'editar/:id', component: EditarLibroComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibroRoutingModule { }
