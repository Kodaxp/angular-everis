import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LibrosService } from '../libros.service';
import { Libros } from '../libros';
import { Imagen } from '../imagen';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-libro',
  templateUrl: './crear-libro.component.html',
  styleUrls: ['./crear-libro.component.css']
})
export class CrearLibroComponent implements OnInit {

  public formularioNuevoLibro: FormGroup;

  public titulo:FormControl;
  public precio:FormControl;
  public idioma:FormControl;
  public isbn:FormControl;
  public descripcion:FormControl;
  public idImagen:FormControl;
  public cantidad:FormControl;
  public correo:FormControl;
  public idiomas:String[];


  constructor(private libroService:LibrosService, private router:Router) { 
    this.titulo = new FormControl('', Validators.required);
    this.precio = new FormControl('', Validators.required);
    this.idioma = new FormControl('', Validators.required);
    this.isbn = new FormControl('', Validators.required);
    this.descripcion = new FormControl('', Validators.required);
    this.idImagen = new FormControl('', Validators.required);
    this.cantidad = new FormControl('', Validators.required);
    this.correo = new FormControl('', Validators.required);
    this.idiomas = ["Español", "Ingles"];
  }

  ngOnInit() {
    this.formularioNuevoLibro = new FormGroup({
      titulo: this.titulo,
      precio: this.precio,
      idioma: this.idioma,
      isbn: this.isbn,
      descripcion: this.descripcion,
      idImagen: this.idImagen,
      cantidad: this.cantidad,
      correo: this.correo
    });
  }

  ingresarLibro(){
    if (this.formularioNuevoLibro.valid) {
      let libro = new Libros();
      let imagen = new Imagen();
      libro.titulo = this.titulo.value;
      libro.precio = this.precio.value;
      libro.idioma = this.idioma.value;
      libro.ISBN = this.isbn.value;
      libro.descripcion = this.descripcion.value;
      libro.cantidad = this.cantidad.value;
      imagen.imagen = this.idImagen.value
      this.libroService.nuevaImagen(imagen).subscribe(
        (respuesta) => {
          console.log(respuesta);
          libro.imagen = respuesta.id;
          this.libroService.nuevoLibro(libro).subscribe(
            (respuesta) => {
              console.log(respuesta);
              this.router.navigate(['libro/listar']);
            }
          ); 
        }
      );
    }
  }

}
