import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Libros } from './libros';
import { Imagen } from './imagen';

@Injectable()
export class LibrosService {

  public URL_API = "http://localhost:3004/libros";
  public URL_API_IMG = "http://localhost:3004/imagen";

  constructor(private httpClient: HttpClient) { }


  listadoDeLibros():Observable<Libros[]>{
    return this.httpClient.get<Libros[]>(this.URL_API);
  }

  nuevoLibro(libro:Libros):Observable<Libros[]>{
    return this.httpClient.post<Libros[]>(this.URL_API, libro);
  }

  eliminarLibro(id:Number):Observable<Libros>{
    return this.httpClient.delete<Libros>(this.URL_API + '/' + id);
  }

  listadoDeLibrosPorID(id):Observable<Libros>{
    return this.httpClient.get<Libros>(this.URL_API + '/' + id);
  }

  editarLibros(libro:Libros): Observable<Libros>{
    return this.httpClient.put<Libros>(this.URL_API + '/' + libro.id, libro);
  }

  nuevaImagen(imagen:Imagen):Observable<Imagen>{
    return this.httpClient.post<Imagen>(this.URL_API_IMG, imagen);
  }
}
