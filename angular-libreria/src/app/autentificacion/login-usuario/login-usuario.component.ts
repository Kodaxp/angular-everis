import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../usuario';
import { AutentificacionService } from '../autentificacion.service';
import { AutentificacionStaticService } from '../autentificacion-static.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-usuario',
  templateUrl: './login-usuario.component.html',
  styleUrls: ['./login-usuario.component.css']
})
export class LoginUsuarioComponent implements OnInit {

  public formularioLogin:FormGroup;

  public usuario: FormControl;
  public contrasena: FormControl;

  constructor(private servicio:AutentificacionService, private router:Router) { 
    this.usuario = new FormControl('', Validators.required);
    this.contrasena = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formularioLogin = new FormGroup({
      usuario: this.usuario,
      contrasena: this.contrasena
    });
  }

  loginUsuario(){
    if (this.formularioLogin.valid) {
      let usuario = new Usuario();
      usuario.usuario = this.usuario.value;
      usuario.contrasena = this.contrasena.value;
      console.log(usuario);
      this.servicio.login(usuario).subscribe(
        (respuesta) => {
          usuario = respuesta;
          if (usuario != null) {
            AutentificacionStaticService.guardarUsuarioEnSesion(usuario);
            this.router.navigate(['libro/listar']);
          }
        }
      );
    }
  }

}
