import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginUsuarioComponent } from './login-usuario/login-usuario.component';
import { DatosUsuarioComponent } from './datos-usuario/datos-usuario.component';

const routes: Routes = [
  {path:'login', component: LoginUsuarioComponent},
  {path:'datos', component: DatosUsuarioComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutentificacionRoutingModule { }
