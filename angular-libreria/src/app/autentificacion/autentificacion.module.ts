import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutentificacionRoutingModule } from './autentificacion-routing.module';
import { AutentificacionComponent } from './autentificacion.component';
import { LoginUsuarioComponent } from './login-usuario/login-usuario.component';
import { DatosUsuarioComponent } from './datos-usuario/datos-usuario.component';
import { LogoutUsuarioComponent } from './logout-usuario/logout-usuario.component';
import { AutentificacionService } from './autentificacion.service';
import { AutentificacionStaticService } from './autentificacion-static.service';
import { ReactiveFormsModule } from "@angular/forms";
import { AutentificacionGuard } from './autentificacion.guard';

@NgModule({
  imports: [
    CommonModule,
    AutentificacionRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AutentificacionComponent, LoginUsuarioComponent, DatosUsuarioComponent, LogoutUsuarioComponent],
  providers: [AutentificacionService, AutentificacionStaticService, AutentificacionGuard],
  exports:[LogoutUsuarioComponent]
})
export class AutentificacionModule { }
