import { Component, OnInit } from '@angular/core';
import { AutentificacionStaticService } from '../autentificacion-static.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout-usuario',
  templateUrl: './logout-usuario.component.html',
  styleUrls: ['./logout-usuario.component.css']
})
export class LogoutUsuarioComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  
  cerrarSesion(){
    console.log("Sesion Cerrada");
    AutentificacionStaticService.cerrarSesion();
    this.router.navigate(['login']);
  }
}
