import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutUsuarioComponent } from './logout-usuario.component';

describe('LogoutUsuarioComponent', () => {
  let component: LogoutUsuarioComponent;
  let fixture: ComponentFixture<LogoutUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
