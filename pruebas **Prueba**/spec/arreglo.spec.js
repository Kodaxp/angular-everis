let arregloPares = require("../src/arregloPares");


describe('Pruebas a arreglo que solo muestras los numeros pares ingresados :', function() {
    
    
    describe('prueba de metodo de pares', function() {
        
        it('solo pares | numeros pares e impares | retorna solo pares', function() {
            const arreglo = [1,2,3,4,5,6,7,8];
            expect(arregloPares.soloPares(arreglo)).toEqual([2,4,6,8]);
        });

        it('solo pares | numeros impares | retorna arreglo vacio', function() {
            const arreglo = [1,3,5,7,9];
            expect(arregloPares.soloPares(arreglo)).toEqual([]);
        });

        it('solo pares | arreglo vacio | retorna arreglo vacio', function() {
            const arreglo = [];
            expect(arregloPares.soloPares(arreglo)).toEqual([]);
        });

        it('solo pares | numeros pares | retorna mismo arreglo con numeros pares', function() {
            const arreglo = [2,4,6,8];
            expect(arregloPares.soloPares(arreglo)).toEqual([2,4,6,8]);
        });

    });
        
        
});
    