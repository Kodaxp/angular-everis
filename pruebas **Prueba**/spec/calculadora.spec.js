let calculadora = require('../src/calculadora');

describe("Pruebas de calculadora:", () => {
    describe("Pruebas de suma:", function() {
        it("suma | de dos numeros positivos y |retorna la suma de estos", function () {
            const primerNumero = 12; 
            const segundoNumero = 5;
            let resultado = calculadora.suma(primerNumero, segundoNumero);
            expect(resultado).toBe(17);
        });
        
        it("suma de dos numeros negativos y retorna la suma de estos", function () {
            const primerNumero = -12; 
            const segundoNumero = -5;
            let resultado = calculadora.suma(primerNumero, segundoNumero);
            expect(resultado).toBe(-17);
        });

        it("suma de un numero positivo y un numero negativo retorna la suma de estos", function () {
            const primerNumero = 12; 
            const segundoNumero = -5;
            let resultado = calculadora.suma(primerNumero, segundoNumero);
            expect(resultado).toBe(7);
        });

        it("suma de dos numeros iguales de distinto signo retorna cero", function () {
            const primerNumero = 5; 
            const segundoNumero = -5;
            let resultado = calculadora.suma(primerNumero, segundoNumero);
            expect(resultado).toBe(0);
        });
    });

    describe("Pruebas de resta:", function() {
        it("resta dos numeros positivos con minuendo mayor retorna como resultado un numero positivo", function () {
            const minuendo = 12; 
            const sustraendo = 5;
            let resultado = calculadora.resta(minuendo, sustraendo);
            expect(resultado).toBe(7);
        });
        
        it("resta minuendo y sustraendo iguales resultado cero ", function () {
            const minuendo = 12; 
            const sustraendo = 12;
            let resultado = calculadora.resta(minuendo, sustraendo);
            expect(resultado).toBe(0);
        });
    });

    describe("Prueba de multiplicacion:", ()=>{
        it("multiplicando dos numeros positivos retornando como resultado la multiplicacion de estos", function() {
            const multiplicando = 2;
            const multiplicador = 2;
            let resultado = calculadora.multiplicacion(multiplicando, multiplicador);
            expect(resultado).toBe(4);
        });

        it("multiplicando dos numeros negativos retornando como resultado la multiplicacion de estos", function() {
            const multiplicando = -2;
            const multiplicador = -2;
            let resultado = calculadora.multiplicacion(multiplicando, multiplicador);
            expect(resultado).toBe(4);
        });

        it("multiplicando dos numeros de diferente signo (positivo y negativo) dando como resultado valor negativo", function() {
            const multiplicando = 2;
            const multiplicador = -2;
            let resultado = calculadora.multiplicacion(multiplicando, multiplicador);
            expect(resultado).toBe(-4);
        });

        it("multiplicando un numero por 0 siendo el resultado igual a 0", function() {
            const multiplicando = 6;
            const multiplicador = 0;
            let resultado = calculadora.multiplicacion(multiplicando, multiplicador);
            expect(resultado).toBe(0);
        });

        it("multiplicando un numero por 1 siendo el resultado igual al numero ingresado", function() {
            const multiplicando = 6;
            const multiplicador = 1;
            let resultado = calculadora.multiplicacion(multiplicando, multiplicador);
            expect(resultado).toBe(6);
        });
    });

    describe('Pruebas de division:', function() {
        it('division de dos numero positivos, dando como resultado la division de estos', () => {
            const dividendo = 4;
            const divisor = 2;
            let resultado = calculadora.division(dividendo, divisor);
            expect(resultado).toBe(2);
        });

        it('division de dos numero negativos, dando como resultado la division de estos', () => {
            const dividendo = -4;
            const divisor = -2;
            let resultado = calculadora.division(dividendo, divisor);
            expect(resultado).toBe(2);
        });

        it('division de dos numero de diferente signo, dando como resultado la division de estos', () => {
            const dividendo = -4;
            const divisor = 2;
            let resultado = calculadora.division(dividendo, divisor);
            expect(resultado).toBe(-2);
        });

        it('division entre 0 y cuaquier numero, dando como resultado 0', () => {
            const dividendo = 0;
            const divisor = 4;
            let resultado = calculadora.division(dividendo, divisor);
            expect(resultado).toBe(0);
        });

        it('division entre algun numero y 1, dando como resultado el numero dividendo', () => {
            const dividendo = 4;
            const divisor = 1;
            let resultado = calculadora.division(dividendo, divisor);
            expect(resultado).toBe(4);
        });
    });
});