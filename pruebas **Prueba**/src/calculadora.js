let calculadora = {
    "suma" : (primerNumero, segundoNumero) => {
        return primerNumero + segundoNumero;
    },
    "resta" : (minuendo, sustraendo) => {
        return minuendo - sustraendo;
    },
    "division" : (dividendo, divisor) => {
        return dividendo / divisor;
    },
    "multiplicacion" : (multiplicando, multiplicador) => {
        return multiplicando * multiplicador;
    }
};

module.exports = calculadora;