let arregloNumeros = {
    "soloPares": (arreglo) =>{
        let arregloCompleto = [];
        arreglo.forEach(numero => {
            if (numero % 2 == 0) {
                arregloCompleto.push(numero);
            }
        });
        return arregloCompleto;
    }
}

module.exports = arregloNumeros;