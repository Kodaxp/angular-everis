import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { KataRoutingModule } from './kata-routing.module';
import { KataComponent } from './kata.component';
import { CrearKataComponent } from './crear-kata/crear-kata.component';

@NgModule({
  imports: [
    CommonModule,
    KataRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [KataComponent, CrearKataComponent]
})
export class KataModule { }
