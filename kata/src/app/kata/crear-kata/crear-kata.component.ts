import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { log } from 'util';

@Component({
  selector: 'app-crear-kata',
  templateUrl: './crear-kata.component.html',
  styleUrls: ['./crear-kata.component.css']
})
export class CrearKataComponent implements OnInit {

  public nombre: FormControl;
  public formularioNombre:FormGroup;
  public valor:String;

  constructor() {
    this.nombre = new FormControl('', Validators.required);
    this.valor = "";
   }

  ngOnInit() {
    this.formularioNombre = new FormGroup({
      nombre: this.nombre
    });
  }

  agregarNombre(){
    this.valor = this.nombre.value;
    console.log(this.valor);
    
  }

}
