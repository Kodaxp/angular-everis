import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearKataComponent } from './crear-kata.component';

describe('CrearKataComponent', () => {
  let component: CrearKataComponent;
  let fixture: ComponentFixture<CrearKataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearKataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearKataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
