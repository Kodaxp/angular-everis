import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearKataComponent } from './crear-kata/crear-kata.component';

const routes: Routes = [
  {path:'', component: CrearKataComponent},
  {path:'kata', children:[
    {path:'crear', component:CrearKataComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KataRoutingModule { }
