import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoDeUsuariosComponent } from './listado-de-usuarios/listado-de-usuarios.component';

const routes: Routes = [
  {path:'', component: ListadoDeUsuariosComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PruebasUsuarioRoutingModule { }
