import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-listado-de-usuarios',
  templateUrl: './listado-de-usuarios.component.html',
  styleUrls: ['./listado-de-usuarios.component.css']
})
export class ListadoDeUsuariosComponent implements OnInit {

  public listadoUsuarios:Usuario[];

  constructor() { 
    this.listadoUsuarios = [];
  }

  ngOnInit() {
  }

  agregarAlListado(usuario){
    if (null != usuario) {
      this.listadoUsuarios.push(usuario);
    }
  }

}
