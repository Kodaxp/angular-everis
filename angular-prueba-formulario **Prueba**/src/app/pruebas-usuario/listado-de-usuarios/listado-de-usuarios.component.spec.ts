import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoDeUsuariosComponent } from './listado-de-usuarios.component';
import { FormularioNuevoUsuarioComponent } from '../formulario-nuevo-usuario/formulario-nuevo-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { Usuario } from '../usuario';

describe('ListadoDeUsuariosComponent', () => {
  let component: ListadoDeUsuariosComponent;
  let fixture: ComponentFixture<ListadoDeUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoDeUsuariosComponent, FormularioNuevoUsuarioComponent ],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoDeUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('agregarAlListado | ingreso de un usuario no nulo| retorna el arreglo del usuario no nulo', () => {
    let usuario = new Usuario();
    component.agregarAlListado(usuario);
    expect(component.listadoUsuarios.length).toBe(1);
  });

  it('agregarAlListado | ingreso de un usuario nulo| retorna el arreglo del usuario no aumenta', () => {
    component.agregarAlListado(null);
    expect(component.listadoUsuarios.length).toBe(0);
  });
});
