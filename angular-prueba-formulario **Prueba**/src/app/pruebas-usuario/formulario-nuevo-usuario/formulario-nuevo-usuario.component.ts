import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-formulario-nuevo-usuario',
  templateUrl: './formulario-nuevo-usuario.component.html',
  styleUrls: ['./formulario-nuevo-usuario.component.css']
})
export class FormularioNuevoUsuarioComponent implements OnInit {

  @Output()
  agregarNuevoUsuario: EventEmitter<Usuario> = new EventEmitter<Usuario>();

  public formularioUsuario: FormGroup;

  public nombre:FormControl;
  public apellido:FormControl;
  public edad:FormControl;
  public usuario:FormControl;

  public nuevoUsuario:Usuario;

  constructor() { }

  ngOnInit() {
    this.nombre = new FormControl('', Validators.required);
    this.apellido = new FormControl('', Validators.required);
    this.edad = new FormControl('', Validators.required);
    this.usuario = new FormControl('', Validators.required);

    this.formularioUsuario = new FormGroup({
      nombre: this.nombre,
      apellido: this.apellido,
      edad: this.edad,
      usuario: this.usuario      
    });
  }

  crearUsuario(){
    if (this.formularioUsuario.valid) {
      this.nuevoUsuario = new Usuario();
      this.nuevoUsuario.nombre = this.nombre.value;
      this.nuevoUsuario.apellido = this.apellido.value;
      this.nuevoUsuario.edad = this.edad.value;
      this.nuevoUsuario.usuario = this.usuario.value;
      this.formularioUsuario.reset();

      this.agregarNuevoUsuario.emit(this.nuevoUsuario);
    }
  }

}
