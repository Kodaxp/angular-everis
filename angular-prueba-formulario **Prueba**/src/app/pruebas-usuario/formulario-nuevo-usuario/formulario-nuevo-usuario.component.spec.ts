import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioNuevoUsuarioComponent } from './formulario-nuevo-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('FormularioNuevoUsuarioComponent', () => {
  let component: FormularioNuevoUsuarioComponent;
  let fixture: ComponentFixture<FormularioNuevoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ReactiveFormsModule],
      declarations: [ FormularioNuevoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioNuevoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('crearUsuario | valores sin ingresar | genera formulario invalido', () => {
    component.nombre.setValue('');
    expect(component.nombre.errors.required).toBeTruthy();
    component.apellido.setValue('');
    expect(component.apellido.errors.required).toBeTruthy();
    component.edad.setValue('');   
    expect(component.edad.errors.required).toBeTruthy(); 
    component.usuario.setValue('');
    expect(component.usuario.errors.required).toBeTruthy();
    expect(component.formularioUsuario.valid).toBeFalsy();
  });

  it('crearUsuario | valores ingresar | genera formulario valido con datos de usuario', () => {
    component.nombre.setValue('Joel');
    component.apellido.setValue('Zuniga');
    component.edad.setValue(21);
    component.usuario.setValue('Koda');
    expect(component.formularioUsuario.valid).toBeTruthy();
    component.crearUsuario();
    expect(component.nuevoUsuario.nombre).toBe('Joel');
    expect(component.nuevoUsuario.apellido).toBe('Zuniga');
    expect(component.nuevoUsuario.edad).toBe(21);
    expect(component.nuevoUsuario.usuario).toBe('Koda');
  });

  it('crearUsuario | usuario por defecto | genera usuario nulo', () => {
    component.crearUsuario();
    expect(component.formularioUsuario.valid).toBeFalsy();
    expect(component.nuevoUsuario).toBeUndefined();
  });

  it('crearUsuario | valores ingresar | crear usuario y emite', () => {
    component.nombre.setValue('Joel');
    component.apellido.setValue('Zuniga');
    component.edad.setValue(21);
    component.usuario.setValue('Koda');
    expect(component.formularioUsuario.valid).toBeTruthy();

    let usuario = null;
    component.agregarNuevoUsuario.subscribe(
      (usuarioEmitido) => {
        usuario = usuarioEmitido;
      }
    );

    component.crearUsuario();

    expect(usuario.nombre).toBe('Joel');
    expect(usuario.apellido).toBe('Zuniga');
    expect(usuario.edad).toBe(21);
    expect(usuario.usuario).toBe('Koda');
  });
});
