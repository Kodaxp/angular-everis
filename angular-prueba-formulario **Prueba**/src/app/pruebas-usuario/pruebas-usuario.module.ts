import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PruebasUsuarioRoutingModule } from './pruebas-usuario-routing.module';
import { FormularioNuevoUsuarioComponent } from './formulario-nuevo-usuario/formulario-nuevo-usuario.component';

import { ReactiveFormsModule } from "@angular/forms";
import { ListadoDeUsuariosComponent } from './listado-de-usuarios/listado-de-usuarios.component';

@NgModule({
  imports: [
    CommonModule,
    PruebasUsuarioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FormularioNuevoUsuarioComponent, ListadoDeUsuariosComponent]
})
export class PruebasUsuarioModule { }
