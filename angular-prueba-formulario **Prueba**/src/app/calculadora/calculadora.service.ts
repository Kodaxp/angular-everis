import { Injectable } from '@angular/core';

@Injectable()
export class CalculadoraService {

  constructor() { }

  static suma(calculadora): number{
    return calculadora.primerNumero + calculadora.segundoNumero;
  }

  static resta(calculadora): number{
    return calculadora.primerNumero - calculadora.segundoNumero;
  }

}
