import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaComponent } from './resta.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CalculadoraService } from '../calculadora.service';

describe('RestaComponent', () => {
  let component: RestaComponent;
  let fixture: ComponentFixture<RestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaComponent ],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('resta de dos numeros positivos que den como resultado un numero negativo', () => {
    component.primerNumero.setValue(10);
    component.segundoNumero.setValue(5);
    spyOn(CalculadoraService, 'resta').and.returnValue(5);
    component.resta();
    expect(component.totalSuma).toBe(5);
  });

  it('resta de dos numeros positivos que den como resultado un numero negativo', () => {
    component.primerNumero.setValue(-10);
    component.segundoNumero.setValue(-5);
    spyOn(CalculadoraService, 'resta').and.returnValue(-5);
    component.resta();
    expect(component.totalSuma).toBe(-5);
  });

  it('resta de un numero positivo y uno negativo que den como resultado un numero negativo', () => {
    component.primerNumero.setValue(-10);
    component.segundoNumero.setValue(5);
    spyOn(CalculadoraService, 'resta').and.returnValue(-15);
    component.resta();
    expect(component.totalSuma).toBe(-15);
  });

  it('resta cuando no se ingresan numeros', () => {
    component.primerNumero.setValue('');
    component.segundoNumero.setValue('');
    spyOn(CalculadoraService, 'resta').and.returnValue(null);
    component.resta();
    expect(component.totalSuma).toBeUndefined();
  });
});
