import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Calculadora } from '../calculadora';
import { CalculadoraService } from '../calculadora.service';

@Component({
  selector: 'app-resta',
  templateUrl: './resta.component.html',
  styleUrls: ['./resta.component.css']
})
export class RestaComponent implements OnInit {

  public formularioResta:FormGroup;

  public primerNumero:FormControl;
  public segundoNumero:FormControl;

  public calculadora:Calculadora;
  public totalSuma:Number;

  constructor() { }

  ngOnInit() {
    this.primerNumero = new FormControl('', Validators.required);
    this.segundoNumero = new FormControl('', Validators.required);

    this.formularioResta = new FormGroup({
      primerNumero: this.primerNumero,
      segundoNumero: this.segundoNumero
    });
  }

  resta(){
    if (this.formularioResta.valid) {
      this.calculadora = new Calculadora();
      this.calculadora.primerNumero = this.primerNumero.value;
      this.calculadora.segundoNumero = this.segundoNumero.value;

      this.totalSuma =  CalculadoraService.resta(this.calculadora);
      this.formularioResta.reset();
    }
  }

}
