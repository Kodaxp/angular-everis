import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalculadoraRoutingModule } from './calculadora-routing.module';
import { SumaComponent } from './suma/suma.component';
import { ReactiveFormsModule } from "@angular/forms";
import { CalculadoraService } from './calculadora.service';
import { RestaComponent } from './resta/resta.component';

@NgModule({
  imports: [
    CommonModule,
    CalculadoraRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SumaComponent, RestaComponent],
  providers: [CalculadoraService]
})
export class CalculadoraModule { }
