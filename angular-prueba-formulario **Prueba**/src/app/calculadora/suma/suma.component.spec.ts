import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SumaComponent } from './suma.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CalculadoraService } from '../calculadora.service';

describe('SumaComponent', () => {
  let component: SumaComponent;
  let fixture: ComponentFixture<SumaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SumaComponent ],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SumaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('suma de dos numero positivos que den como resultado un numero positivo', () => {
    component.primerNumero.setValue(10);
    component.segundoNumero.setValue(5);
    spyOn(CalculadoraService, 'suma').and.returnValue(15);
    component.suma();
    expect(component.totalSuma).toEqual(15);
  });

  it('suma de dos numeros positivos que den como resultado un numero negativo', () => {
    component.primerNumero.setValue(-10);
    component.segundoNumero.setValue(-5);
    spyOn(CalculadoraService, 'resta').and.returnValue(-15);
    component.suma();
    expect(component.totalSuma).toBe(-15);
  });

  it('suma de un numero positivo y uno negativo que den como resultado un numero negativo', () => {
    component.primerNumero.setValue(-10);
    component.segundoNumero.setValue(5);
    spyOn(CalculadoraService, 'resta').and.returnValue(-5);
    component.suma();
    expect(component.totalSuma).toBe(-5);
  });

  it('suma cuando no se ingresan numeros', () => {
    component.primerNumero.setValue('');
    component.segundoNumero.setValue('');
    spyOn(CalculadoraService, 'resta').and.returnValue(null);
    component.suma();
    expect(component.totalSuma).toBeUndefined();
  });
});
