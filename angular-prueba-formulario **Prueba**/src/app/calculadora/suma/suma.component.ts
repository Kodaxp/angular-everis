import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CalculadoraService } from '../calculadora.service';
import { Calculadora } from '../calculadora';

@Component({
  selector: 'app-suma',
  templateUrl: './suma.component.html',
  styleUrls: ['./suma.component.css']
})
export class SumaComponent implements OnInit {

  public formularioSuma:FormGroup;

  public primerNumero:FormControl;
  public segundoNumero:FormControl;

  public calculadora:Calculadora;
  public totalSuma:Number;

  constructor() { }

  ngOnInit() {
    this.primerNumero = new FormControl('', Validators.required);
    this.segundoNumero = new FormControl('', Validators.required);

    this.formularioSuma = new FormGroup({
      primerNumero: this.primerNumero,
      segundoNumero: this.segundoNumero
    });
  }

  suma(){
    if (this.formularioSuma.valid) {
      this.calculadora = new Calculadora();
      this.calculadora.primerNumero = this.primerNumero.value;
      this.calculadora.segundoNumero = this.segundoNumero.value;

      this.totalSuma =  CalculadoraService.suma(this.calculadora);
      this.formularioSuma.reset();
    }
  }

}
