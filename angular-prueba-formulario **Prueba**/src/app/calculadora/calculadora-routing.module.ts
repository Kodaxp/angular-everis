import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SumaComponent } from './suma/suma.component';
import { RestaComponent } from './resta/resta.component';

const routes: Routes = [
  {path:'suma', component:SumaComponent},
  {path:'resta', component:RestaComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalculadoraRoutingModule { }
