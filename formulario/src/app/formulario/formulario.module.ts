import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms'


import { FormularioRoutingModule } from './formulario-routing.module';
import { FormularioComponent } from '../formulario/formulario.component';

@NgModule({
  imports: [
    CommonModule,
    FormularioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FormularioComponent]
})
export class FormularioModule { }
