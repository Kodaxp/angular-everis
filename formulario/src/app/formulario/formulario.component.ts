import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms'; 
import { Usuario } from './usuario';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public formUsuario: FormGroup;
  public usuario:Usuario;

  constructor() { }

  ngOnInit() {
    this.formUsuario = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(3)]),
      edad: new FormControl(0, Validators.required)
    });
  }

  crearUsuario(){
    if(this.formUsuario.valid){
      console.log(this.formUsuario.value);
      console.log(this.formUsuario.controls.nombre.errors.minLength);
      this.formUsuario.reset();
    }
  }

}
