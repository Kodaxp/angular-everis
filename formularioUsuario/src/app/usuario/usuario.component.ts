import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario } from './usuario';
import { log } from 'util';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  @ViewChild('formUsuario') formUsuario:any;

  public usuario:Usuario;
    
  constructor() { 
    this.usuario = new Usuario();
  }

  crearUsuario(){
    if (this.formUsuario.valid) {
      console.log(this.usuario);
      this.formUsuario.reset();
    }
  }

  ngOnInit() {
  }

}
