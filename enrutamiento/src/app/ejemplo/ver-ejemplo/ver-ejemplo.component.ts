import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from "@angular/router";

@Component({
  selector: 'app-ver-ejemplo',
  templateUrl: './ver-ejemplo.component.html',
  styleUrls: ['./ver-ejemplo.component.css']
})
export class VerEjemploComponent implements OnInit {

  public id:String;

  constructor(
    private route:ActivatedRoute,
    private router: Router
  ) {}

  //inyección de dependencia se usa para hyacer un mejor manejo de memoria a la hora de la creación de instancias por el patron Singleton

  ngOnInit() {
    this.route.params.forEach((params:Params) => {
      this.id = params["id"];
    });
    console.log("id :: "+ this.id)
  }

  redireccionar(){
    this.router.navigate(['/ejemplo/inicio']);
  }

}
