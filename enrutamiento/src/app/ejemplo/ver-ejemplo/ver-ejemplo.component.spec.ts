import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEjemploComponent } from './ver-ejemplo.component';

describe('VerEjemploComponent', () => {
  let component: VerEjemploComponent;
  let fixture: ComponentFixture<VerEjemploComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerEjemploComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEjemploComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
