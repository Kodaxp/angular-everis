import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EjemploComponent } from './ejemplo.component';
import { VerEjemploComponent } from './ver-ejemplo/ver-ejemplo.component';
import { SobreEjemploComponent } from './sobre-ejemplo/sobre-ejemplo.component';

const routes: Routes = [
  {path:'', redirectTo:'ejemplo/inicio', pathMatch:'full'},
  {path:'ejemplo', children: [
    {path:'inicio', component: EjemploComponent},
    //{path:'inicio/:id', component: EjemploComponent}, // envia una id por el link, espera un parametro despues del /
    {path:'ver', component: VerEjemploComponent},
    {path:'ver/:id', component: VerEjemploComponent},
    {path:'sobre', component: SobreEjemploComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjemploRoutingModule { }
