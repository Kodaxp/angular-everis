import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EjemploRoutingModule } from './ejemplo-routing.module';
import { EjemploComponent } from './ejemplo.component';
import { VerEjemploComponent } from './ver-ejemplo/ver-ejemplo.component';
import { SobreEjemploComponent } from './sobre-ejemplo/sobre-ejemplo.component';

@NgModule({
  imports: [
    CommonModule,
    EjemploRoutingModule
  ],
  declarations: [EjemploComponent, VerEjemploComponent, SobreEjemploComponent],
  exports: [EjemploComponent, VerEjemploComponent, SobreEjemploComponent]
})
export class EjemploModule { }
