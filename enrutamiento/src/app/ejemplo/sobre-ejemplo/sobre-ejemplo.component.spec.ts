import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SobreEjemploComponent } from './sobre-ejemplo.component';

describe('SobreEjemploComponent', () => {
  let component: SobreEjemploComponent;
  let fixture: ComponentFixture<SobreEjemploComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SobreEjemploComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SobreEjemploComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
