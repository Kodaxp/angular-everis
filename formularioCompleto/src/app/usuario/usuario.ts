export class Usuario {
    public nombre:String;
    public edad:number;
    public sexo:String;

    constructor(nombre,edad,sexo){
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo
    }
}
