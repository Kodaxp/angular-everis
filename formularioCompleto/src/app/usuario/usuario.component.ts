import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario } from './usuario';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  @ViewChild('formUsuarios') formUsuarios:any;

  public usuario:Usuario;
  public sexoArreglo = ["Seleccionar...", "Masculine", "Femenine", "Otre"];

  constructor() {
    this.usuario = new Usuario('', 0, '');
  }


  crearUsuario(){
    if (this.formUsuarios.valid) {
      console.log(this.usuario);
      this.formUsuarios.reset();
    }
  }

  ngOnInit() {
  }

}
