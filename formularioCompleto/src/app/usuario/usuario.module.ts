import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';

import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    FormsModule
  ],
  declarations: [UsuarioComponent],
  exports: [UsuarioComponent]
})
export class UsuarioModule { }
