import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-usuario',
  templateUrl: './listar-usuario.component.html',
  styleUrls: ['./listar-usuario.component.css']
})
export class ListarUsuarioComponent implements OnInit {

  public usuarios:Usuario[];

  constructor(private usuarioService:UsuarioService, private router:Router) {}

  ngOnInit() {
    this.usuarioService.listadoUsuario().subscribe(respuesta =>{
      this.usuarios = respuesta;
    });
  }

  eliminarUsuario(id){
    if (id > 0) {
      this.usuarioService.eliminarUsuario(id).subscribe(respuesta =>{
        console.log(respuesta);
      })
    }
  }

  redireccionarUsuario(id){
    this.router.navigate(['/user/edit-users/',id]);
    console.log(id);
  }

}
