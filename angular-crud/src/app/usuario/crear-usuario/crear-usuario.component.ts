import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  public formularioUsuario:FormGroup;

  public nombre:FormControl;
  public apellidoPaterno:FormControl;
  public apellidoMaterno:FormControl;
  public edad:FormControl;
  public email:FormControl;
  public deportes:String[];

  public usuario:Usuario;

  constructor(private usaurioService:UsuarioService) { 
    this.nombre = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.maxLength(15)]);
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*')]);
    this.apellidoMaterno = new FormControl('', Validators.pattern('[a-zA-Z]*'));
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
    this.email = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]);
    
  }

  ngOnInit() {
    this.formularioUsuario = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      edad: this.edad,
      email: this.email
    });
  }

  ingresarUsuario(){
    if (this.formularioUsuario.valid) {
      this.usuario = new Usuario();
      this.usuario.nombre = this.nombre.value;
      this.usuario.apellidoPaterno = this.apellidoPaterno.value;
      this.usuario.apellidoMaterno = this.apellidoMaterno.value;
      this.usuario.edad = this.edad.value;
      this.usuario.email = this.email.value;
      console.log(this.usuario);
      this.usaurioService.guardarUsuario(this.usuario).subscribe(respuesta => {
        console.log(respuesta);
        this.formularioUsuario.reset();
      });
    }
  }
}
