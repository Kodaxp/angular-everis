import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';

const routes: Routes = [{path:'', component:ListarUsuarioComponent},
{path:'user', children:[
  {path:'create-users', component: CrearUsuarioComponent},
  {path:'list-users', component: ListarUsuarioComponent},
  {path:'edit-users/:id', component: EditarUsuarioComponent}
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
