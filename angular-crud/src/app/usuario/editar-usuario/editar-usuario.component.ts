import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

  public id:Number;
  public usuario:Usuario;
  public formularioEdicionUsuario: FormGroup;

  public nombre:FormControl;
  public apellidoPaterno:FormControl;
  public apellidoMaterno:FormControl;
  public edad:FormControl;
  public email:FormControl;

  constructor(private activatedRouter: ActivatedRoute, private usuarioService:UsuarioService,
    private router:Router) { 
    this.nombre = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.maxLength(15)]);
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*')]);
    this.apellidoMaterno = new FormControl('', Validators.pattern('[a-zA-Z]*'));
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
    this.email = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]);
  }

  ngOnInit() {
    this.activatedRouter.params.forEach((params: Params) => {
      console.log(params['id']);
      this.id = params['id'];
    });
    
    this.formularioEdicionUsuario = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      edad: this.edad,
      email : this.email
    });

    this.usuarioService.listadoUsuarioPorId(this.id).subscribe(respuesta => {
      this.usuario = respuesta;
      console.log(this.usuario);

      this.nombre.setValue(this.usuario.nombre);
      this.apellidoPaterno.setValue(this.usuario.apellidoPaterno);
      this.apellidoMaterno.setValue(this.usuario.apellidoMaterno);
      this.edad.setValue(this.usuario.edad);
      this.email.setValue(this.usuario.email);
    });
  }

  editarUsuario(){
    if (this.formularioEdicionUsuario.valid) {
      this.usuario.nombre = this.nombre.value;
      this.usuario.apellidoPaterno = this.apellidoPaterno.value;
      this.usuario.apellidoMaterno = this.apellidoMaterno.value;
      this.usuario.edad = this.edad.value;
      this.usuario.email = this.email.value;
      console.log(this.usuario);
      this.usuarioService.editarUsaurio(this.usuario).subscribe(respuesta =>{
        console.log(respuesta);
        this.formularioEdicionUsuario.reset();
        this.router.navigate(['user/list-users']);
      });
    }
  }

  redireccionarListado(){
    this.formularioEdicionUsuario.reset();
    this.router.navigate(['user/list-users']);  }

}
