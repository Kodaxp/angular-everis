import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { UsuarioService } from './usuario.service';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';

@NgModule({
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [UsuarioComponent, CrearUsuarioComponent, ListarUsuarioComponent, EditarUsuarioComponent],
  providers: [UsuarioService]
})
export class UsuarioModule { }
