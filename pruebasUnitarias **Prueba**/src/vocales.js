let vocalesDentroDeUnString = {
    "contarVocales": (palabraARevisar) => {
        // let arregloPalabra = palabraARevisar.split('');
        // return arregloPalabra.reduce( (totalVocal, elemento) => {
        //     if (elemento == "a" || elemento == "e" || elemento == "i" || elemento == "o" || elemento == "u") {
        //         return ++totalVocal;
        //     }
        //     else{
        //         return totalVocal;
        //     }
        // }, 0
        // );

        let contador = 0;
        palabraARevisar = palabraARevisar.replace(/ /g, '');
        for (let index = 0; index < palabraARevisar.length; index++) {
            let vocalesARevisar = "aeiouAEIOU";
            if (vocalesARevisar.indexOf(palabraARevisar[index]) !== -1) {
                contador += 1;
            }
        }
        return contador;
    },

    "voltearUnaPalabra": (palabraAVoltear) => {
        if (palabraAVoltear == null) {
            return null;
        }
        return palabraAVoltear.split('').reverse().join('');

        // let palabraVolteada = "";
        // let palabra = palabraAVoltear.length;
        // while (palabra >= 0) {
        //     palabraVolteada += palabraAVoltear.charAt(palabra);
        //     palabra--;
        // }
        // return palabraVolteada;
    },

    "soloLetras": (palabraIngresada) => {
        let validador = true;
        palabraIngresada = palabraIngresada.replace(/ /g, '');
        for (let index = 0; index < palabraIngresada.length; index++) {
            let vocalesARevisar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (vocalesARevisar.indexOf(palabraIngresada[index]) === -1) {
                validador = false;
            }
        }
        return validador;
    }
}

module.exports = vocalesDentroDeUnString;