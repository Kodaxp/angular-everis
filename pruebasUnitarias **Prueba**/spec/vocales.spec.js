let vocalesDentroDeUnArreglo = require('../src/vocales');

describe('pruebas para sacar las vocales de un String', () => {
    describe('prueba vocales dentro de un string', () => {
        it('contar | vocales dentro de un string de 14 letras | retorna la cantidad de vocales dentro del string ingresado', () => {
            const palabraAIngresar = "paralelepipedo";
            let resultado = vocalesDentroDeUnArreglo.contarVocales(palabraAIngresar);
            expect(resultado).toBe(7);
        })

        it('contar | vocales dentro de un string de 4 letras | retorna la cantidad de vocales dentro del string ingresado', () => {
            const palabraAIngresar = "hola";
            let resultado = vocalesDentroDeUnArreglo.contarVocales(palabraAIngresar);
            expect(resultado).toBe(2);
        })

        it('contar | vocales dentro de un string donde no se encuentren vocales | retorna una cantidad de 0 al no encontrar vocales', () => {
            const palabraAIngresar = "lllllqqqqqq";
            let resultado = vocalesDentroDeUnArreglo.contarVocales(palabraAIngresar);
            expect(resultado).toBe(0);
        })

        it('contar | vocales dentro de un string donde solo se encuentren vocales | retorna la cantidad total de la palabra', () => {
            const palabraAIngresar = "aaaaaeeeeoui";
            let resultado = vocalesDentroDeUnArreglo.contarVocales(palabraAIngresar);
            expect(resultado).toBe(12);
        })
    });

    describe('pruebas para el volteo de un String', () => {
        it('voltearUnaPalabra | un string de 4 palabras | retornando el string inverso', () => {
            const palabraAVoltear = "hola";
            let resultado = vocalesDentroDeUnArreglo.voltearUnaPalabra(palabraAVoltear);
            expect(resultado).toBe("aloh");
        })

        it('voltearUnaPalabra | un string de 14 palabras | retornando el string inverso', () => {
            const palabraAVoltear = "paralelepipedo";
            let resultado = vocalesDentroDeUnArreglo.voltearUnaPalabra(palabraAVoltear);
            expect(resultado).toBe("odepipelelarap");
        })

        it('voltearUnaPalabra | un string de con espaciosss | retornando el string inverso', () => {
            const palabraAVoltear = "asdf sss";
            let resultado = vocalesDentroDeUnArreglo.voltearUnaPalabra(palabraAVoltear);
            expect(resultado).toBe("sss fdsa");
        })

        it('voltearUnaPalabra | un string vacio | retornando una variable vacia', () => {
            const palabraAVoltear = "";
            let resultado = vocalesDentroDeUnArreglo.voltearUnaPalabra(palabraAVoltear);
            expect(resultado).toBe("");
        })

        it('voltearUnaPalabra | se ingresa un null | retornando una variable null', () => {
            const palabraAVoltear = null;
            let resultado = vocalesDentroDeUnArreglo.voltearUnaPalabra(palabraAVoltear);
            expect(resultado).toBeNull();
        })
    })

    describe('pruebas a solo ingreso de letras', () => {
        it('solo letras | ingreso de una palabra que solo contenga letras | retorna un true por solo obtener letras', () => {
            const palabraIngresada = "murcielago";
            let resultado = vocalesDentroDeUnArreglo.soloLetras(palabraIngresada);
            expect(resultado).toBe(true);
        });

        it('solo letras | ingreso de una palabra con numeros | retorna un false por tener numeros', () => {
            const palabraIngresada = "hola1234hola";
            let resultado = vocalesDentroDeUnArreglo.soloLetras(palabraIngresada);
            expect(resultado).toBe(false);
        });

        it('solo letras | ingreso de una palabra solo con numeros | retorna un false por tener numeros', () => {
            const palabraIngresada = "12345";
            let resultado = vocalesDentroDeUnArreglo.soloLetras(palabraIngresada);
            expect(resultado).toBe(false);
        });

        it('solo letras | ingreso de una palabra solo con numeros | retorna un false por tener numeros', () => {
            const palabraIngresada = "1ajoa";
            let resultado = vocalesDentroDeUnArreglo.soloLetras(palabraIngresada);
            expect(resultado).toBe(false);
        });
    });
});