import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const NOMBRE_USUARIO = 'usuario';

@Injectable()
export class LoginUsuarioService {

  public URL_API = "http://localhost:3004/usuario";
  

  constructor(private httpClient: HttpClient) { }

  loginUsuario(usuario: Usuario){
    let httpParams = new HttpParams();
    httpParams = httpParams.set('contrasena', usuario.contrasena.toString());
    httpParams = httpParams.set('usuario', usuario.usuario.toString());
    return this.httpClient.get<Usuario[]>(this.URL_API , {params:httpParams});
  }

  guardarUsuario(usuario: Usuario):Observable<Usuario>{
    return this.httpClient.post<Usuario>(this.URL_API, usuario);
  }

  buscarUsuarios():Observable<Usuario[]>{
    return this.httpClient.get<Usuario[]>(this.URL_API);
  }

  static guardarEnSesion(name:string, object:string){
    localStorage.setItem(name,object);
  }

  static guardarUsuarioEnSesion(id:Number){
    this.guardarEnSesion(NOMBRE_USUARIO, JSON.stringify(id));
    return true;
  }

}
