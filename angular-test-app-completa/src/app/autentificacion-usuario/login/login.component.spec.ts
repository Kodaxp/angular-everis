import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginUsuarioService } from '../login-usuario.service';
import { Usuario } from '../usuario';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loginservice:LoginUsuarioService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    loginservice = TestBed.get(LoginUsuarioService);
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

  it('loginUsuario, ingreso de un usuario valido, retorna valor true', () => {
    component.usuario.setValue('jzuniga');
    component.contrasena.setValue('12345');
    spyOn(LoginUsuarioService, 'guardarUsuarioEnSesion').and.returnValue(true);
    expect(component.usuarioGuardadoEnSesion).toBeTruthy();
  });
});
