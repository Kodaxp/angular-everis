import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../usuario';
import { LoginUsuarioService } from '../login-usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formularioLogin:FormGroup;

  public usuario:FormControl;
  public contrasena:FormControl;
  public nuevoUsuario:Usuario;
  public usuarioGuardadoEnSesion:boolean;

  constructor(private usuarioService:LoginUsuarioService, private router:Router) { }

  ngOnInit() {
    this.usuario = new FormControl('', Validators.required);
    this.contrasena = new FormControl('', Validators.required);

    this.formularioLogin = new FormGroup({
      usuario: this.usuario,
      contrasena: this.contrasena
    });
  }

  loginUsuario(){
    if (this.formularioLogin.valid) {
      this.nuevoUsuario = new Usuario();
      this.nuevoUsuario.usuario = this.usuario.value;
      this.nuevoUsuario.contrasena = this.contrasena.value;
      this.usuarioService.loginUsuario(this.nuevoUsuario).subscribe(
        (respuesta) => {
          if (respuesta.length > 0) {
            this.nuevoUsuario = respuesta[0];
            if (this.nuevoUsuario != null) {
              console.log(LoginUsuarioService.guardarUsuarioEnSesion(this.nuevoUsuario.id));
              
              LoginUsuarioService.guardarUsuarioEnSesion(this.nuevoUsuario.id);
              this.router.navigate(['crearUsuario']);
            }
          }
        }
      );
    }
  }

}
