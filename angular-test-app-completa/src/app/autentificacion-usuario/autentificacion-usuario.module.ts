import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutentificacionUsuarioRoutingModule } from './autentificacion-usuario-routing.module';
import { LoginComponent } from './login/login.component';
import { FormularioNuevoUsuarioComponent } from './formulario-nuevo-usuario/formulario-nuevo-usuario.component';
import { ReactiveFormsModule } from "@angular/forms";
import { LoginUsuarioService } from './login-usuario.service';

@NgModule({
  imports: [
    CommonModule,
    AutentificacionUsuarioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [LoginComponent, FormularioNuevoUsuarioComponent],
  providers: [LoginUsuarioService]
})
export class AutentificacionUsuarioModule { }
