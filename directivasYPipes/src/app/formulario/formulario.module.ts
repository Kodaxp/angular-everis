import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';

import { FormularioRoutingModule } from './formulario-routing.module';
import { FormularioComponent } from './formulario.component';
import { ValidacionEdadPipe } from './validacion-edad.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormularioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FormularioComponent, ValidacionEdadPipe]
})
export class FormularioModule { }
