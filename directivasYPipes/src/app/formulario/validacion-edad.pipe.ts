import { Pipe, PipeTransform } from '@angular/core';
import { log } from 'util';

@Pipe({
  name: 'validacionEdad'
})
export class ValidacionEdadPipe implements PipeTransform {

  private MAYOR_DE_EDAD:Number = 18;

  transform(value: any): any {
    console.log("pasa por la pipe");
    if (value >= this.MAYOR_DE_EDAD) {
      return 'Es mayor de edad';
    }else{
      return 'Es menor de edad';
    }
  }
}