import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public formUsuario: FormGroup;
  public sexo:String[];
  public listado:any[];

  constructor() { }

  ngOnInit() {
    this.formUsuario = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(3)]),
      edad: new FormControl('', Validators.required),
      sexo: new FormControl('', Validators.required)
    });
    this.sexo = ["Masculine", "Femenine", "Otre"];
    this.listado=[];
  }

  listadoUsuarios(){
    if (this.formUsuario.valid) {
      this.listado.push(this.formUsuario.value);
      console.log(this.listado);
      this.formUsuario.reset();
    }
  }
}
