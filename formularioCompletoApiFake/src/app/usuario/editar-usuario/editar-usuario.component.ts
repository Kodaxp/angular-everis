import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

  private id:Number;
  private usuario: Usuario;

  public formularioUsuariosAEditar: FormGroup;
  public nombre: FormControl;
  public apellidoPaterno: FormControl;
  public apellidoMaterno: FormControl;
  public edad: FormControl;
  public email: FormControl;

  constructor(private activatedRoute: ActivatedRoute, private usuarioService:UsuarioService) { 
    this.nombre = new FormControl(this.usuario.nombre ,[Validators.required, Validators.maxLength(15), Validators.pattern('[a-zA-z]*')]); //validación letras
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-z]*')]);
    this.apellidoMaterno = new FormControl('', Validators.pattern('[a-zA-z]*'));
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]); // validación solo numeros
    this.email = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]')]); //validación de email
   }

  ngOnInit() {
    this.activatedRoute.params.forEach((params:Params) => {
      console.log(params['id']);
      this.id = params['id']
    })

    this.usuarioService.listarUsuarioPorID(this.id).subscribe((respuesta) => {
      this.usuario = respuesta;
      console.log(this.usuario);
      //se copia el formulario en los value se coloca el nombre del objeto
    });

    this.formularioUsuariosAEditar = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno : this.apellidoMaterno,
      edad : this.edad,
      email : this.email
    });
  }

  editarUsuario(){
      
  }
}
