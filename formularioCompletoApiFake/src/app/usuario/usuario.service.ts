import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './usuario';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UsuarioService {

  private URL_API: string = "http://localhost:3004/usuarios";

  constructor(
    private httpClient: HttpClient
  ) { }

  guardarUsuario(usuario: Usuario): Observable<Usuario>{
    return this.httpClient.post<Usuario>(this.URL_API, usuario);
  }

  listadoUsuario(): Observable<Usuario[]>{
    return this.httpClient.get<Usuario[]>(this.URL_API);
  }

  listarUsuarioPorID(id:Number): Observable<Usuario>{
    return this.httpClient.get<Usuario>(this.URL_API + '/' + id);
  }

  eliminarUsaurio(usuario:Usuario): Observable<Usuario>{
    return this.httpClient.delete<Usuario>(this.URL_API + '/'+ usuario.id)
  }

  editarUsuario(usuario:Usuario): Observable<Usuario[]>{
    return this.httpClient.put<Usuario[]>(this.URL_API + '/'+ usuario.id, usuario)
  }
}
