export class Usuario {
    public id: Number;
    public nombre:String;
    public apellidoPaterno:String;
    public apellidoMaterno:String;
    public edad:Number;
    public email:String;

    cargarUsuario(nombre:String, apellidoPaterno:String, edad:Number, email:String, apellidoMaterno:String){
        this.nombre = nombre,
        this.apellidoPaterno = apellidoPaterno,
        this.apellidoMaterno = apellidoMaterno;
        this.edad = edad,
        this.email = email
    }
}
