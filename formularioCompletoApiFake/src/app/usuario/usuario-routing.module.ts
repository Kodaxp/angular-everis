import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ListadoUsuarioComponent } from './listado-usuario/listado-usuario.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';

const routes: Routes = [
  {path:'', component: ListadoUsuarioComponent},
  {path:'usuarios', children :[
    {path:'listadoUsuario', component: ListadoUsuarioComponent},
    {path:'crearUsuario', component: CrearUsuarioComponent},
    {path:'editarUsuario/:id', component: EditarUsuarioComponent
  }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
