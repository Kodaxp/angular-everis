import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listado-usuario',
  templateUrl: './listado-usuario.component.html',
  styleUrls: ['./listado-usuario.component.css'],
  providers: [UsuarioService]
})
export class ListadoUsuarioComponent implements OnInit {

  public listadoDeUsuarios:Usuario[];

  constructor(
    private usuarioService: UsuarioService,
    private router: Router
  ) {}

  ngOnInit() {
    this.usuarioService.listadoUsuario().subscribe(respuesta =>{
      this.listadoDeUsuarios = respuesta;
    })
  }

  eliminarUsuario(id){
    console.log(id);
    if (id > 0) {
      let usuario = new Usuario();
      usuario.id = id;
      this.usuarioService.eliminarUsaurio(usuario).subscribe(respuesta => {
        console.log(respuesta);
      });
    }
  }

  redireccionarAEditar(id){
    this.router.navigate(['usuarios/editarUsuario/', id])
  }

  agregarAlListado(usuarios){
    console.log(usuarios);
    this.listadoDeUsuarios.push(usuarios);
    console.log(this.listadoDeUsuarios);
  }
}
