import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../usuario';
import { UsuarioService } from "../usuario.service";

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  @Output()
  agregarUsuario: EventEmitter<Usuario> = new EventEmitter<Usuario>();

  public formularioUsuarios: FormGroup;
  private usuario: Usuario;

  public nombre: FormControl;
  public apellidoPaterno: FormControl;
  public apellidoMaterno: FormControl;
  public edad: FormControl;
  public email: FormControl;

  constructor(
    private usuarioService : UsuarioService
  ) { 
    this.nombre = new FormControl('', [Validators.required, Validators.maxLength(15), Validators.pattern('[a-zA-z]*')]); //validación letras
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-z]*')]);
    this.apellidoMaterno = new FormControl('', Validators.pattern('[a-zA-z]*'));
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]); // validación solo numeros
    this.email = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]); //validación de email
  }

  ngOnInit() {
    this.formularioUsuarios = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno : this.apellidoMaterno,
      edad : this.edad,
      email : this.email
    });
  }

  crearUsuario(){
    if (this.formularioUsuarios.valid) {
      this.usuario = new Usuario();
      // this.usuario = this.formularioUsuarios.value; no funciona
      this.usuario.nombre = this.nombre.value;
      this.usuario.apellidoPaterno = this.apellidoPaterno.value;
      this.usuario.apellidoMaterno = this.apellidoMaterno.value;
      this.usuario.edad = this.edad.value;
      this.usuario.email = this.email.value;
      console.log(this.usuario);
      this.usuarioService.guardarUsuario(this.usuario).subscribe(respuesta => {
        console.log(respuesta);
        this.formularioUsuarios.reset();
      });

      this.agregarUsuario.emit(this.usuario);
    }
  }

}
