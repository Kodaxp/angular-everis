import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ReactiveFormsModule } from "@angular/forms";
import { UsuarioService } from './usuario.service';
import { ListadoUsuarioComponent } from './listado-usuario/listado-usuario.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';

@NgModule({
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [UsuarioComponent, CrearUsuarioComponent, ListadoUsuarioComponent, EditarUsuarioComponent],
  exports: [UsuarioComponent, CrearUsuarioComponent],
  providers: [UsuarioService]
})
export class UsuarioModule { }
