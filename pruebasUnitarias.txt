Pruebas Unitarias
====================================
¿Que son?

    Es una forma de comprobar el correcto funcionamiento de una pieza de código a traves de aserciones o espectativas

        Automatizable                   --> Corre de manera automatica
        Completas                       --> Tomas la completitud del metodo que le damos
        Repetibleso o Reoutilizables    --> No puede correr solo vax
        Independientes                  --> Sin relacion cn ifs+
        Profesionale                    --> seguir las normas, dar status de profesionalismo

    Ventajas
        Fomentane l cambio              --> Mejora continua
        Simplifica la ointegracion      --> mas entendible
        Documenta el codigo
        Reduce los costos               --> visualizar problemas antes de produccion 
        Calidad de codigo               --> mejorar el codigo

JASMINE
================================================
¿que es?

    framework de BDD (behavior driven development)
    no depende de ninguna otra libreria    
    no requiere el DOM

¿Como instalar?

    creamos un proyecto node                                    --> npm init -f
    agregamos la dependencia de jasmine                         --> npm install --save-dev jasmine
    instalamos Jasmine de manera global para inicializar        --> npm install -g jasmine
    inicializamos jasmine                                       --> jasmine init

    agregamos el script para iniciar las pruebas en "scripts"   --> "test-init":"node ./node_modules/jasmin/bin/jasmine.js init"
                                                                --> npm run test-init
    agregamos el script para correr las pruebas en "scripts"    --> "test":"node ./node_modules/jasmin/bin/jasmine.js"
                                                                --> npm test

¿Como se usa?

    "a" es la descripcion de texto y "b" funcion anonima de los test --> descrive(a,b)
    "a" es una descripcion de la prueba y "b" funcion con expectativas a cumplir --> it(a,b)
    "a" es el valor a evaluar mediante argumentos en cadena     --> expect(a)
    Ejemplos                                                    --> expect(a).not.toBe(false); (no falso)
                                                                --> expect(a).not.toBeDefined(); (no undefined)
                                                                --> expect(a).not.toBeGreaterThanOrEqual(25);
                                                                --> expect(a).not.toBeNull();

    nombre del metodo                                           --> sumaDeCincoNumerosEnteros()
    que es lo que vamos a poner a prueba                        --> 5 y 2
    cual es el resultado que nosotros esperamos                 --> 7

    antes de todas las pruebas unitarias                        --> beforeAll(function(), timeout)
    se ejecuta antes de cada prueba unitaria                    --> beforeEach(function(), timeout)

    se ejecuta despues de todas las pruebas unitarias --> afterAll(function(), timeout)
    se ejecuta despues de cada prueba unitaria --> afterEach(function(), timeout)
    

