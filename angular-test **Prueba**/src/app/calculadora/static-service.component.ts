import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-service',
  templateUrl: './static-service.component.html',
  styleUrls: ['./static-service.component.css']
})
export class StaticServiceComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  static sumarDosNumeros(numeroUno:number, numeroDos:number){
    let valor = numeroUno + numeroDos;
    return valor;
  }

  static restarDosNumeros(numeroUno:number, numeroDos:number){
    let valor = numeroUno - numeroDos;
    return valor;
  }

}
