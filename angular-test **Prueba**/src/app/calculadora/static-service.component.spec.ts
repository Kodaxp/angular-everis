import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticServiceComponent } from './static-service.component';

describe('StaticServiceComponent', () => {
  let component: StaticServiceComponent;
  let fixture: ComponentFixture<StaticServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
