import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculadoraComponent } from './calculadora.component';
import { StaticServiceComponent } from '../static-service.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('CalculadoraComponent', () => {
  let component: CalculadoraComponent;
  let fixture: ComponentFixture<CalculadoraComponent>;
  let debugElementSuma:DebugElement;
  let debugElementResta:DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraComponent ],
      providers:[StaticServiceComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculadoraComponent);
    component = fixture.componentInstance;
    debugElementSuma = fixture.debugElement.query(By.css('#suma'));
    debugElementResta = fixture.debugElement.query(By.css('#resta'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sumaDeDosNumero | ingreso de dos numeros positivos | retorno de un numero positivo', () => {
    spyOn(StaticServiceComponent, 'sumarDosNumeros').and.returnValue(50);
    fixture.detectChanges();
    expect(debugElementSuma.nativeElement.innerText.trim()).toBe('50');
  });

  it('sumaDeDosNumero | ingreso de dos numeros positivos | retorno de un numero positivo', () => {
    spyOn(StaticServiceComponent, 'restarDosNumeros').and.returnValue(10);
    fixture.detectChanges();
    expect(debugElementResta.nativeElement.innerText.trim()).toBe('10');
  });
});
