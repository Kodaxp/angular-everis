import { Component, OnInit } from '@angular/core';
import { StaticServiceComponent } from '../static-service.component';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {
  
  constructor() {}

  ngOnInit() {
  }

  sumaDeDosNumero(parametroUno:number, parametroDos:number){
    return StaticServiceComponent.sumarDosNumeros(parametroUno, parametroDos);
  }

  restaDeDosNumeros(parametroUno:number, parametroDos:number){
    return StaticServiceComponent.restarDosNumeros(parametroUno, parametroDos);
  }

}
