import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalculadoraRoutingModule } from './calculadora-routing.module';
import { CalculadoraComponent } from './calculadora/calculadora.component';
import { StaticServiceComponent } from './static-service.component';

@NgModule({
  imports: [
    CommonModule,
    CalculadoraRoutingModule
  ],
  declarations: [CalculadoraComponent, StaticServiceComponent]
})
export class CalculadoraModule { }
