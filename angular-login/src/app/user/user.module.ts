import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { ComunService } from './comun.service';
import { UserServiceService } from './user-service.service';
import { LoginGuard } from './login.guard';
import { ListUserComponent } from './list-user/list-user.component';
import { CreateUserComponent } from './create-user/create-user.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [UserComponent, LoginUserComponent, ListUserComponent, CreateUserComponent],
  exports:[UserComponent],
  providers: [ComunService, UserServiceService, LoginGuard]
})
export class UserModule { }
