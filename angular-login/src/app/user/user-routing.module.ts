import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginUserComponent } from './login-user/login-user.component';
import { LoginGuard } from './login.guard';
import { ListUserComponent } from './list-user/list-user.component';
import { CreateUserComponent } from './create-user/create-user.component';

const routes: Routes = [
  {path:'', component: LoginUserComponent},
  {path:'user', children:[
    {path:'login', component: LoginUserComponent},
    {path:'create',canActivate:[LoginGuard], component: CreateUserComponent},
    {path:'list', component: ListUserComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
