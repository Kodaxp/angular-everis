import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../user';
import { UserServiceService } from '../user-service.service';
import { ComunService } from '../comun.service';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent implements OnInit {

  public formLoginUser: FormGroup;
    
  public email:FormControl;
  public password:FormControl;

  constructor(private userService: UserServiceService) { 
    this.email = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]);
    this.password = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formLoginUser = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  loginUser(){
    if (this.formLoginUser.valid) {
      console.log(this.formLoginUser.value);
      let user = new User();
      user.email = this.email.value;
      user.password = this.password.value;
      console.log(user);
      this.userService.userLogin(user).subscribe(respuesta => {
        user = respuesta;
        console.log(user);
        if (user != null) {
          user.password = "";
          ComunService.guardarUsuarioEnSesion(user);
        }
      });
    }
  }

}
