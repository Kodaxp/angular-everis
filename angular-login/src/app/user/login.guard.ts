import { Injectable } from '@angular/core';
import { CanActivate} from '@angular/router';
import { ComunService } from './comun.service';

@Injectable()
export class LoginGuard implements CanActivate {
  canActivate(): boolean {
    if (null != ComunService.buscarUsuarioEnSesion()) {
      console.log("posee sesión activa o permisos");
      return true;
    }
    console.log("no posee sesión activa o permisos");
    return false;
  }
}
