import { Injectable } from '@angular/core';
import { User } from './user';

const USER_KEY = 'user';

@Injectable()
export class ComunService {

  constructor() { }


  // CONFIRMACION DE LOGIN

  static guardarEnSesion(name:string, object:string){
    localStorage.setItem(name,object);
  }

  static guardarUsuarioEnSesion(user: User){
    this.guardarEnSesion(USER_KEY, JSON.stringify(user));
  }


  // GUARD INICIO DE SESION
  static buscarUsuarioEnSesion(): User{
    if (null == this.buscaEnSesion(USER_KEY)) {
      return null;
    }
    let user = new User();
    user = JSON.parse(this.buscaEnSesion(USER_KEY));
    return user;
  }

  static buscaEnSesion(nombre: string): string{
    return localStorage.getItem(nombre);
  }

  //CERRAR SESION  

  static cerrarSesion(){
    localStorage.clear();
  }

}
