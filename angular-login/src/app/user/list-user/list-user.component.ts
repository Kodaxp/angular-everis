import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { User } from '../user';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  public listadoUsuarios: User[];

  constructor(private userService:UserServiceService) { }

  ngOnInit() {
    this.userService.buscarUsuarios().subscribe((respuesta) => {
      this.listadoUsuarios = respuesta;
      console.log(respuesta);
    });
  }

}
