import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from 'rxjs/Observable';
import { HttpParams, HttpClient } from "@angular/common/http";


@Injectable()
export class UserServiceService {

  public user:User;
  public URL = "http://localhost:3004/usuarios";

  constructor(private httpClient: HttpClient) { }

  userLogin(user: User){
    let httpParams = new HttpParams();
    httpParams = httpParams.set('password', user.password.toString());
    httpParams = httpParams.set('email', user.email.toString());
    console.log(httpParams.toString());
    return this.httpClient.get<User>(this.URL , {params:httpParams});
  }

  guardarUsuario(user:User):Observable<User>{
    return this.httpClient.post<User>(this.URL, user);
  }

  buscarUsuarios():Observable<User[]>{
    return this.httpClient.get<User[]>(this.URL);
  }

}
