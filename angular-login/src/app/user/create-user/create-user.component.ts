import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../user';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  public formNewUser: FormGroup;

  public email: FormControl;
  public password: FormControl;

  constructor(private userService: UserServiceService, private router:Router) { 
    this.email = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]);
    this.password = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formNewUser = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  createUser(){
    if (this.formNewUser.valid) {
      let user = new User();
      user.email = this.email.value;
      user.password = this.password.value;

      this.userService.guardarUsuario(user).subscribe((respuesta) =>{
        console.log(respuesta);
        this.formNewUser.reset();
      });
      this.router.navigate(['user/list']);
    }
  }

}
