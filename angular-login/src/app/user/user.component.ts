import { Component, OnInit } from '@angular/core';
import { ComunService } from './comun.service';
import { Router } from '@angular/router';
import { User } from './user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  logout(){
    console.log("Sesion Cerrada");
    ComunService.cerrarSesion();
    this.router.navigate(['user/login']);
  }

}
